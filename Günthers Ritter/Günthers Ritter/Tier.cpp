#include "Tier.h"
#include <iostream>
#include <string>

using namespace std;

string Tier::getName()
{
	return Name;
}

int Tier::getLebenskraft()
{
	return Lebenskraft;
}

void Tier::setLebenskraft(int lebenskraft)
{
	Lebenskraft = lebenskraft;
}

bool Tier::getGesinnung()
{
	return Gesinnung;
}

void Tier::setGesinnung(bool gesinnung)
{
	Gesinnung = gesinnung;
}

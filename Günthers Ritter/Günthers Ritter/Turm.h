#pragma once
#include "Drache.h"
#include "Mensch.h"


class Turm
{
private:
	Drache *Wachdrache;
	Mensch *Gefangener;
public:
	Drache* getWachdrache();
	void setWachdrache(Drache* wachdrache);
	Mensch* getGefangener();
	void setGefangener(Mensch* gefangener);
	
	Turm(Drache *wachedrache, Mensch *gefangener) { 
		Wachdrache = wachedrache;
		Gefangener = gefangener;
	}
};


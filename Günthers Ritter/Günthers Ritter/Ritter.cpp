#include <iostream>
#include <string>
#include "Ritter.h"
#include "Mensch.h"

using namespace std;

int Ritter::getX()
{
	return X;
}

void Ritter::setX(int x)
{
	X = x;
}

int Ritter::getY()
{
	return Y;
}

void Ritter::setY(int y)
{
	Y = y;
}

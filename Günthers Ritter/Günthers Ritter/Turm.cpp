#include "Turm.h"
#include "Drache.h"
#include "Mensch.h"

Drache* Turm::getWachdrache()
{
	return  Wachdrache;
}

void Turm::setWachdrache(Drache* wachdrache)
{
	Wachdrache = wachdrache;
}

Mensch* Turm::getGefangener()
{
	return Gefangener;
}

#pragma once
#include <iostream>
#include <string>
#include "Mensch.h"

using namespace std;
class Ritter : Mensch
{
private:
	int Kampfkraft;
	string Rüstungsfarbe;
	int X;
	int Y;
public:
	int getX();
	void setX(int x);
	int getY();
	void setY(int y);
	Ritter(string name, int lebenskraft, int kampfkraft, int x, int y) : Mensch(name, lebenskraft) {
		Kampfkraft = kampfkraft;
		X = x;
		Y = y;
	}
};
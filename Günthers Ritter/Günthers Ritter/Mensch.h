#pragma once
#include <string>
using namespace std;

class Mensch
{
private:
	string Name;
	int Lebenskraft;

public:
	string getName();
	int getLebenskraft();
	void setLebenskraft(int lebenskraft);
	
	Mensch(string name, int lebenskraft) {
		Name = name;
		Lebenskraft = lebenskraft;
	}
};


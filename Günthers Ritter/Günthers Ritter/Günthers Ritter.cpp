#include <iostream>
#include <string>
#include "Ritter.h"
#include "Turm.h"
#include "Drache.h"
#include "Mensch.h"

using namespace std;

Turm *TurmMap[3][3];
Ritter *RitterMap[3][3];

int iLaufen(Ritter *spieler)
{
	char eingabe;
	cout << "wohin wollen sie gehen?" << endl;
	cout << "\t w\n\ta s d" << endl;
	cin >> eingabe;
	if(eingabe == 'w')
	{
		if(TurmMap[spieler->getY() - 1][spieler->getX()] != nullptr && spieler->getY() - 1 >= 0)
		{
			RitterMap[spieler->getY()][spieler->getX()] = nullptr;
			spieler->setY(spieler->getY() - 1);
			RitterMap[spieler->getY()][spieler->getX()] = spieler;

			cout << "Du bist auf einen Turm gesto�en?" << endl;
		}else if (spieler->getY() - 1 >= 0)
		{
			RitterMap[spieler->getY()][spieler->getX()] = nullptr;
			spieler->setY(spieler->getY() - 1);
			RitterMap[spieler->getY()][spieler->getX()] = spieler;
		}
		else
		{
			cout << "Die Welt ist hier zu Ende" << endl;
		}
	}
	else if(eingabe == 'a')
	{
		if (TurmMap[spieler->getY()][spieler->getX() - 1] != nullptr && spieler->getX() - 1 >= 0)
		{
			RitterMap[spieler->getY()][spieler->getX()] = nullptr;
			spieler->setX(spieler->getX() - 1);
			RitterMap[spieler->getY()][spieler->getX()] = spieler;
			
			cout << "Du bist auf einen Turm gesto�en?" << endl;
		}
		else if(spieler->getX() - 1 >= 0)
		{
			RitterMap[spieler->getY()][spieler->getX()] = nullptr;
			spieler->setX(spieler->getX() - 1);
			RitterMap[spieler->getY()][spieler->getX()] = spieler;
		}
		else
		{
			cout << "Die Welt ist hier zu Ende" << endl;
		}
	}
	else if (eingabe == 's')
	{
		if (TurmMap[spieler->getY() + 1][spieler->getX()] != nullptr && spieler->getY() + 1 <= sizeof(RitterMap)/sizeof(RitterMap[0]))
		{
			RitterMap[spieler->getY()][spieler->getX()] = nullptr;
			spieler->setY(spieler->getY() + 1);
			RitterMap[spieler->getY()][spieler->getX()] = spieler;
			
			cout << "Du bist auf einen Turm gesto�en?" << endl;
		}
		else if(spieler->getY() + 1 <= sizeof(RitterMap) / sizeof(RitterMap[0]))
		{
			RitterMap[spieler->getY()][spieler->getX()] = nullptr;
			spieler->setY(spieler->getY() + 1);
			RitterMap[spieler->getY()][spieler->getX()] = spieler;
		}
		else
		{
			cout << "Die Welt ist hier zu Ende" << endl;
		}
	}
	else if (eingabe == 'd')
	{
		if (TurmMap[spieler->getY()][spieler->getX() + 1] != nullptr && spieler->getX() + 1 <= sizeof(RitterMap[0])/sizeof(RitterMap[0][0]))
		{
			RitterMap[spieler->getY()][spieler->getX()] = nullptr;
			spieler->setX(spieler->getX() + 1);
			RitterMap[spieler->getY()][spieler->getX()] = spieler;
			
			cout << "Du bist auf einen Turm gesto�en?" << endl;
		}
		else if (spieler->getX() + 1 <= sizeof(RitterMap[0]))
		{
			RitterMap[spieler->getY()][spieler->getX()] = nullptr;
			spieler->setX(spieler->getX() + 1);
			RitterMap[spieler->getY()][spieler->getX()] = spieler;
		}
		else
		{
			cout << "Die Welt ist hier zu Ende" << endl;
		}
	}
	return 0;
}

int iController() {
	bool ende = false;
	TurmMap[0][0] = new Turm(&Drache("roter Drache", 20, 10, false), &Mensch("Prinzessin", 10));
	TurmMap[0][2] = new Turm(&Drache("blauer Drache", 20, 10, true), &Mensch("kleiner Junge", 10));
	Ritter Spieler("Ritter", 40, 20, 1, 2);
	RitterMap[Spieler.getY()][Spieler.getX()] = &Spieler;
	do
	{
		iLaufen(&Spieler);
	} while (!ende);


	
	return 0;
}

int main()
{
	iController();
	return 0;
}

